package UD20Game1.UD20Game1;

import methods.*;
import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Game1App extends JFrame {

	public JPanel contentPane;
	public static JTextField j1Input;
	public static JTextField j2Input;
	public static JLabel turnPlayer;
	public static JLabel endMessage;
	
	
	public static int[][] matriz = new int[3][3];
	
	public static final JButton aa = new JButton();
	public static final JButton ab = new JButton();
	public static final JButton ac = new JButton();
	public static final JButton ba = new JButton();
	public static final JButton bb = new JButton();
	public static final JButton bc = new JButton();
	public static final JButton ca = new JButton();
	public static final JButton cb = new JButton();
	public static final JButton cc = new JButton();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Game1App frame = new Game1App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Game1App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 488);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, aa, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, aa, 6, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, aa, 145, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, aa, 172, SpringLayout.WEST, contentPane);
		contentPane.add(aa);
		aa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aa.setIcon(Methods.turn);
				matriz[0][0] = Methods.turnNum;
				Methods.switcher(aa);
				Methods.checkers(0,0);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, ab, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, ab, 172, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, ab, 145, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, ab, 338, SpringLayout.WEST, contentPane);
		contentPane.add(ab);
		ab.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ab.setIcon(Methods.turn);
				matriz[0][1] = Methods.turnNum;
				Methods.switcher(ab);
				Methods.checkers(0,1);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, ac, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, ac, 338, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, ac, 145, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, ac, 504, SpringLayout.WEST, contentPane);
		contentPane.add(ac);
		ac.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ac.setIcon(Methods.turn);
				matriz[0][2] = Methods.turnNum;
				Methods.switcher(ac);
				Methods.checkers(0,2);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, ba, 145, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, ba, 6, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, ba, 285, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, ba, 172, SpringLayout.WEST, contentPane);
		contentPane.add(ba);
		ba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ba.setIcon(Methods.turn);
				matriz[1][0] = Methods.turnNum;
				Methods.switcher(ba);
				Methods.checkers(1,0);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, bb, 145, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, bb, 172, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, bb, 285, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, bb, 338, SpringLayout.WEST, contentPane);
		contentPane.add(bb);
		bb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bb.setIcon(Methods.turn);
				matriz[1][1] = Methods.turnNum;
				Methods.switcher(bb);
				Methods.checkers(1,1);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, bc, 145, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, bc, 338, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, bc, 285, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, bc, 504, SpringLayout.WEST, contentPane);
		contentPane.add(bc);
		bc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bc.setIcon(Methods.turn);
				matriz[1][2] = Methods.turnNum;
				Methods.switcher(bc);
				Methods.checkers(1,2);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, ca, 285, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, ca, 6, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, ca, 425, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, ca, 172, SpringLayout.WEST, contentPane);
		contentPane.add(ca);
		ca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ca.setIcon(Methods.turn);
				matriz[2][0] = Methods.turnNum;
				Methods.switcher(ca);
				Methods.checkers(2,0);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, cb, 285, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, cb, 172, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, cb, 425, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, cb, 338, SpringLayout.WEST, contentPane);
		contentPane.add(cb);
		cb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cb.setIcon(Methods.turn);
				matriz[2][1] = Methods.turnNum;
				Methods.switcher(cb);
				Methods.checkers(2,1);
			}
		});
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, cc, 285, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, cc, 338, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, cc, 425, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, cc, 504, SpringLayout.WEST, contentPane);
		contentPane.add(cc);
		cc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cc.setIcon(Methods.turn);
				matriz[2][2] = Methods.turnNum;
				Methods.switcher(cc);
				Methods.checkers(2,2);
			}
		});
		
		
		JButton btnNuevaPartida = new JButton("Nueva Partida");
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnNuevaPartida, 0, SpringLayout.NORTH, aa);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnNuevaPartida, -101, SpringLayout.EAST, contentPane);
		contentPane.add(btnNuevaPartida);
		btnNuevaPartida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Methods.replay();
			}
		});
		
		JLabel j1Label = new JLabel("Jugador 1:");
		sl_contentPane.putConstraint(SpringLayout.NORTH, j1Label, 95, SpringLayout.SOUTH, btnNuevaPartida);
		sl_contentPane.putConstraint(SpringLayout.WEST, j1Label, 51, SpringLayout.EAST, ac);
		sl_contentPane.putConstraint(SpringLayout.EAST, j1Label, -175, SpringLayout.EAST, contentPane);
		contentPane.add(j1Label);
		
		j1Input = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, j1Input, -3, SpringLayout.NORTH, j1Label);
		sl_contentPane.putConstraint(SpringLayout.WEST, j1Input, 6, SpringLayout.EAST, j1Label);
		j1Input.setText("J1");
		contentPane.add(j1Input);
		j1Input.setColumns(10);
		
		JLabel j2Label = new JLabel("Jugador 2:");
		sl_contentPane.putConstraint(SpringLayout.NORTH, j2Label, 137, SpringLayout.SOUTH, j1Label);
		sl_contentPane.putConstraint(SpringLayout.WEST, j2Label, 51, SpringLayout.EAST, bc);
		sl_contentPane.putConstraint(SpringLayout.EAST, j2Label, -175, SpringLayout.EAST, contentPane);
		contentPane.add(j2Label);
		
		j2Input = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, j2Input, -3, SpringLayout.NORTH, j2Label);
		sl_contentPane.putConstraint(SpringLayout.WEST, j2Input, 0, SpringLayout.WEST, j1Input);
		j2Input.setText("J2");
		j2Input.setColumns(10);
		contentPane.add(j2Input);
		
		JLabel turnLabel = new JLabel("Turno:");
		sl_contentPane.putConstraint(SpringLayout.WEST, turnLabel, 51, SpringLayout.EAST, ac);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, turnLabel, -40, SpringLayout.NORTH, j1Label);
		contentPane.add(turnLabel);
		
		turnPlayer = new JLabel(j1Input.getText());
		sl_contentPane.putConstraint(SpringLayout.NORTH, turnPlayer, 0, SpringLayout.NORTH, turnLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, turnPlayer, 49, SpringLayout.EAST, turnLabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, turnPlayer, 0, SpringLayout.EAST, j1Input);
		contentPane.add(turnPlayer);
		
		endMessage = new JLabel("");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, endMessage, -45, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, endMessage, -119, SpringLayout.EAST, contentPane);
		contentPane.add(endMessage);
	}
}
