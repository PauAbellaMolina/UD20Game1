package methods;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import UD20Game1.UD20Game1.Game1App;

public class Methods {
	public static ImageIcon X = new ImageIcon(Game1App.class.getResource("/icons/X.png"));
	public static ImageIcon O = new ImageIcon(Game1App.class.getResource("/icons/O.png"));
	public static ImageIcon turn = O;
	public static int turnNum = 1;
	
	public static void switcher(JButton btn) {
		btn.setEnabled(false);
		
		if (turn == X) {
			turn = O;
			turnNum = 1;
			Game1App.turnPlayer.setText(Game1App.j1Input.getText());
		} else {
			turn = X;
			turnNum = 2;
			Game1App.turnPlayer.setText(Game1App.j2Input.getText());
		}
	}
	
	public static void checkers(int f, int c) {
		checkerFil(f,c);
		checkerCol(f,c);
		checkerDiag(f,c);
	}
	
	public static void checkerFil(int f, int c) {
		if (c == 0) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f][c+1]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f][c+2]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else if (c == 2) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f][c-1]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f][c-2]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else {		
			if(Game1App.matriz[f][c] == Game1App.matriz[f][c-1] && Game1App.matriz[f][c] == Game1App.matriz[f][c+1]) {
				ended(Game1App.matriz[f][c]);
			}
		}
	}
	
	public static void checkerCol(int f, int c) {
		if (f == 0) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f+1][c]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f+2][c]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else if (f == 2) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f-1][c]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f-2][c]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else {		
			if(Game1App.matriz[f][c] == Game1App.matriz[f-1][c] && Game1App.matriz[f+1][c] == Game1App.matriz[f][c]) {
				ended(Game1App.matriz[f][c]);
			}
		}
	}
	
	public static void checkerDiag(int f, int c) {
		if (f == 0 && c == 0) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f+1][c+1]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f+2][c+2]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else if (f == 2 && c == 0) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f-1][c+1]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f-2][c+2]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else if (f == 0 && c == 2) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f+1][c-1]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f+2][c-2]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else if (f == 2 && c == 2) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f-1][c-1]) {
				if(Game1App.matriz[f][c] == Game1App.matriz[f-2][c-2]) {
					ended(Game1App.matriz[f][c]);
				}
			}
		} else if (f == 1 && c == 1) {
			if(Game1App.matriz[f][c] == Game1App.matriz[f-1][c-1] && Game1App.matriz[f+1][c+1] == Game1App.matriz[f][c] || Game1App.matriz[f][c] == Game1App.matriz[f-1][c+1] && Game1App.matriz[f+1][c-1] == Game1App.matriz[f][c]) {
				ended(Game1App.matriz[f][c]);
			}
		}
	}
	
	public static void ended(int player) {
		String winnerName = "";
		if (player == 1) {
			winnerName = Game1App.j1Input.getText();
		} else {
			winnerName = Game1App.j2Input.getText();
		}
		
		Game1App.endMessage.setText("Enhorabuena, ha ganado " + winnerName);
		
		Game1App.aa.setEnabled(false);
		Game1App.ab.setEnabled(false);
		Game1App.ac.setEnabled(false);
		Game1App.ba.setEnabled(false);
		Game1App.bb.setEnabled(false);
		Game1App.bc.setEnabled(false);
		Game1App.ca.setEnabled(false);
		Game1App.cb.setEnabled(false);
		Game1App.cc.setEnabled(false);
	}
	
	public static void replay() {
		Game1App.matriz = new int[3][3];
		
		Game1App.aa.setEnabled(true);
		Game1App.aa.setIcon(null);
		Game1App.ab.setEnabled(true);
		Game1App.ab.setIcon(null);
		Game1App.ac.setEnabled(true);
		Game1App.ac.setIcon(null);
		Game1App.ba.setEnabled(true);
		Game1App.ba.setIcon(null);
		Game1App.bb.setEnabled(true);
		Game1App.bb.setIcon(null);
		Game1App.bc.setEnabled(true);
		Game1App.bc.setIcon(null);
		Game1App.ca.setEnabled(true);
		Game1App.ca.setIcon(null);
		Game1App.cb.setEnabled(true);
		Game1App.cb.setIcon(null);
		Game1App.cc.setEnabled(true);
		Game1App.cc.setIcon(null);
		
		Game1App.endMessage.setText("");
	}
}
